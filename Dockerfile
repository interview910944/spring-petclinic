# ������� �����
ARG BASE_IMAGE="docker.do.neoflex.ru/openjdk:17-alpine"

FROM $BASE_IMAGE

# ��������� ������� ����������
WORKDIR /app

# ����� bash
RUN apk add --no-cache bash

# ����� tzdata ��� ��������� �������� �����
RUN apk add --no-cache tzdata
COPY target/*.jar /app/app.jar

# ����� �����
EXPOSE 8080

#������� �� ���������
ENTRYPOINT ["java", "-jar", "app.jar"]